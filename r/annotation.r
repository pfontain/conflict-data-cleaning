#
# annotation
#
# merges responses with annotations
#
# by Paulo Gomes <pfontain@ucsc.edu>

source("utilities.r")

annotation = new.env()
annotation$labels = c("active","aggressive")
annotation$sourceCorpus = "corpus"
annotation$sourceUserStudy = "user_study"
annotation$source = c(annotation$sourceCorpus, annotation$sourceUseStudy)

# NOTE: User study has less responses and scenarios but supposedly higher confidence on those labels

# NOTE: Response IDs appear to be globally unique
annotation$aggregate = function(a){
	selected_columns = c("scenario_id","feature_val","response_text")

	# WARNING: A "demand the money back" of the same scenario with different response ids
	# This will cause there will be repeated values for it
	a_agg = aggregate(x=a[, selected_columns],by=list(response_id=a$response_id),function(x){util$mode(x)[1]})    
    	for(name in selected_columns)
    		a_agg[,name] = as.factor(a_agg[, name])
    	
    return(a_agg)
}

annotation$load.and.aggregate = function(annotation_feature,source){
	annotation_filename = paste("tmp_annotations", annotation_feature,paste(source,".csv",sep=""),sep="_")
	annotations_file_path = file.path("..","data-formatted", annotation_filename)
	annotations = read.csv(annotations_file_path)
	annotations = annotation$aggregate(annotations)
	names(annotations)[names(annotations) == 'feature_val'] = paste("a",annotation_feature,"mode",sep="_")
	return(annotations)
}

annotation$load = function(source=annotation$sourceCorpus){
	# WARNING: Assuming that all annotationshave the same subset of responses
	Reduce(function(x,y){
				merge(x = x, y = y, by.x=c("scenario_id","response_text","response_id"), by.y=c("scenario_id","response_text","response_id"))},
				lapply(annotation$labels,
						function(l){
							annotation$load.and.aggregate(annotation_feature=l,source=source)}))
}