# 
# utilities.R
#
# Common functions to several scripts mostly to do with
# merging batches.
#
# TODO: fix warnings with plot generate
#
# by Paulo Gomes <pfontain@ucsc.edu>

util = new.env()

# Prints the columns each batch does not have.
# batches: list of data frame batches
util$print.diff <- function(batches){
	set_labels_responses <- Reduce(union,lapply(batches,names))
	for(i in 1:length(batches)){
		empty_fields <- setdiff(set_labels_responses,names(batches[[i]]))
		print(list(empty_fields,","))
	}
}

# Prints the columns that at least one batch does not have.
# batches: list of data frame batches
util$print.diff.union <- function(batches){
	set_labels_responses <- Reduce(union,lapply(batches,names))
	set_diff <- c()
	for(i in 1:length(batches)){
		set_diff <- union(set_diff,setdiff(set_labels_responses,names(batches[[i]])))
	}
	print(set_diff)
}

# Receives a data frame (batch) and returns copy modified such that
# for each column in a vector (selected_column_names) but not in the
# original data frame, it puts a column of missing values. 
#
# batch: data frame batch
# selected_column_names: columns that the generate data frame should have
#
util$batch.add.missing <- function(batch,selected_column_names){
	batch_rewritten <- list()
	number_rows <- length(batch[[1]])
	for(i in 1:length(selected_column_names)){
		position_vector <- which(names(batch)==selected_column_names[i])
		has_column <- length(position_vector)!=0
		# if it has the column add it to the list
		if(has_column){
			position <- position_vector[1]
			batch_rewritten <- append(batch_rewritten,list(batch[,position]))
		}
		# if it does not add an empty column
		else{
			batch_rewritten <- append(batch_rewritten,list(rep(NA, number_rows)))
		}
	}
	# generates frame from list of columns
	batch_rewritten_frame <- data.frame(batch_rewritten)
	for(i in 1:length(selected_column_names)){
		colnames(batch_rewritten_frame)<-selected_column_names
	}
	return(batch_rewritten_frame)
 }
 
# generates all merged variables plots
# graphs are 800 pixel wide and saved as png to out_graphs folder
# TODO: exclude id variables
# TODO: make the size of the graphs a parameter
util$plot.generate <- function(data_merge, graph_default_folder= file.path("..","graphs")) {
	# is this line necessary?
	par(mfrow = c(length(names(data_merge)), 1))

	dev.new()
	for (i in 1:length(names(data_merge))) {
		plot(data_merge[i], col = "tan1")
		file_path = file.path(graph_default_folder,paste(names(data_merge)[i],".png"))
		dev.print(device = png, width = 800, file_path)
	}
}

# returns the mode
# x: vector/list
util$mode <- function(x) {
   temp <- table(as.vector(x))
   names(temp)[temp == max(temp)]
}