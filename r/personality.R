# 
# personality
#
# loading personality instances and mapping numeric values to likert scales
# see README for more info on the data
#
# TODO: Convert values to OCEAN
# TODO: Remove instances with repeated work ids
#
# by Paulo Gomes <pfontain@ucsc.edu>

personality = new.env()
personality$originalBFIColumns = c("bf1_reserved","bf2_trusting","bf3_lazy","bf4_relaxed","bf5_artistic","bf6_outgoing","bf7_fault_others","bf8_thorough","bf9_nervous","bf10_imagination","bf11_kind")

# Loads the generated CSV file into a data frame and returns it
personality$load <- function(file_path){
	personality <- read.csv(file=file_path)
	
	# Remove duplicate entries for the same user
	personality <- personality[!duplicated(personality[c("worker_id")]),]
	
	# BFI questions combined according to [ADD REF] criteria
	personality$bfi_extraversion <- (6-personality$bf1_reserved + personality$bf6_outgoing)/2
	personality$bfi_agreeableness <- (personality$bf2_trusting + 6 - personality$bf7_fault_others + personality$bf11_kind)/3
	personality$bfi_conscientiousness <- (6-personality$bf3_lazy + personality$bf8_thorough)/2
	personality$bfi_neuroticism <- (6-personality$bf4_relaxed + personality$bf9_nervous)/2
	personality$bfi_openness <- (6-personality$bf5_artistic + personality$bf10_imagination)/2
   
    # Removing original BFI question features
    personality <- personality[,!names(personality) %in% personality$originalBFIColumns]
		
	return(personality)
}

# Recommended query: hist(personality$bfi_extraversion)