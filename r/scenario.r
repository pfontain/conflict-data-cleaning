# 
# scenario
#
# loads batch files for original scenarios (phase I)
# and merges them.
#
# TODO: Map Several answer issues to same value
# TODO: add more scenario fields, namely work relationship
#
# by Paulo Gomes <pfontain@ucsc.edu>

# NOTE Answer.Sex has a lot of NAs

source("utilities.R")

scenario = new.env()

scenario$files = c("Batch_603140_batch_results.csv","Batch_603992_batch_results.csv","Batch_605271_batch_results.csv","Batch_606292_batch_results.csv","Batch_607541_batch_results.csv","Batch_607591_batch_results.csv","Batch_609483_batch_results.csv","Batch_617248_batch_results.csv","Batch_619333_batch_results.csv","Batch_620262_batch_results.csv","Batch_620688_batch_results.csv","Batch_624319_batch_results.csv","Batch_626280_batch_results.csv","Batch_635587_batch_results.csv","Batch_674970_batch_results.csv")

scenario$folder = file.path("..","data-raw","scenarios_original")

# List of all columns to be considered in batches of the original scenarios
# INSTRUCTIONS: filter columns by commenting out lines 
# Generated with Reduce(paste,(Reduce(union,lapply(batches,names))))
# NOTE: Title and Description may play a role in the described scenario but that does not matter
#		if we are mainly concerned with useful information for abstracted scenarios.
# NOTE: Input.Instructions is same as Title and Description
# NOTE: Input.ConflictType is missing for most scenarios
# NOTE: Answer.FamilialRelationship is None in most cases (No answer or no familiar relationship?) 
# NOTE: Answer.Sex could be interesting for analyzing the original scenarios but not the abstracted ones.
#		The can not guarantee that the sex will also be different when responders answer about abstracted scenarios.
scenario$selectedColumns <- c(
	# "HITId", "HITTypeId","Title","Description","Keywords", "Reward", "CreationTime", "MaxAssignments", "RequesterAnnotation", "AssignmentDurationInSeconds", "AutoApprovalDelayInSeconds", "Expiration", "NumberOfSimilarHITs", "LifetimeInSeconds",
	 "AssignmentId",
	# "WorkerId","AssignmentStatus", "AcceptTime", "SubmitTime","AutoApprovalTime","ApprovalTime","RejectionTime","RequesterFeedback","WorkTimeInSeconds","LifetimeApprovalRate","Last30DaysApprovalRate","Last7DaysApprovalRate","Input.Instructions","Input.Examples", "Input.ConflictType", "Input.Role Answer.Comment", "Answer.ConflictDescription", "Answer.ContinuedFriendshipDuration","Answer.Emotion", "Answer.FamilialRelationship",
	"Answer.FriendshipType"
	# "Answer.Issues","Answer.PreviousFriendshipDuration","Answer.SameSex","Answer.SchoolRelationship","Answer.Severity","Answer.Sex","Approve","Reject","Answer.WorkRelationship", "Answer.Satisfaction","Answer.TimeLapsed","Answer.Improvement"
	 )

scenario$batch.load.scenarios <- function(scenarioFolder=scenario$folder){
		return(lapply(
					lapply(scenario$files,
							function(filename){ return(file.path(scenarioFolder,filename)) }),
					read.csv))
}

scenario$batch.merge.scenarios <- function(batches,requested_fields){
		Reduce(rbind,
				lapply(batches,
						function(batch){
							return(util$batch.add.missing(batch,requested_fields))}))
}

scenario$sex.map <- function(sex_label){
	if(is.na(sex_label)) NA
	else if(sex_label == "Male") 1
	else if(sex_label == "Female") 2
	else NA
}

scenario$relationship.map <- function(friendship_label){
	if(friendship_label == "Stranger") 1
	else if(friendship_label == "Acquaintance") 2
	else if(friendship_label == "RomanticallyInterested" || friendship_label == "Friend") 3
	else if(friendship_label == "RomanticallyInvolved" || friendship_label == "CloseFriend") 4
	else if(friendship_label == "Spouse") 5
	else NA
}

scenario$involved.map <- function(friendship_label){
	if(friendship_label == "Stranger" ||
		friendship_label == "Acquaintance" ||
		friendship_label == "Friend" ||
		friendship_label == "CloseFriend" ||
		friendship_label == "RomanticallyInterested") 1
	else if(friendship_label == "RomanticallyInvolved" ||
			friendship_label == "Spouse") 2
	else NA
}

scenario$load <- function(){
	data <- scenario$batch.merge.scenarios(scenario$batch.load.scenarios(), scenario$selectedColumns)
	
	names(data)[names(data)=="AssignmentId"] <- "scenario_id"
	
	# Mapping numbers to corresponding labels
	# data$sex_scenario_num <-sapply(data$Answer.Sex, sex.map)
	data$friendship_num <-sapply(data$Answer.FriendshipType,scenario$relationship.map)
	data$involved_num <-sapply(data$Answer.FriendshipType,scenario$involved.map)
	
	# data <- data[,!(names(data) %in% c("Answer.FriendshipType"))]
	#TODO: Rename to relationship field
	names(data)[names(data)=="Answer.FriendshipType"] <- "friendship"
	
	# Removing all instances without relationship data
	data = data[!is.na(data$friendship_num),]
	
	return(data)
}