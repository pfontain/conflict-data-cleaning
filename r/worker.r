# 
# merge_DP
#
# loads demographic and personality instances
# and merges them.See README for more info on the data
#
# by Paulo Gomes <pfontain@ucsc.edu>

source("demographic.r")
source("personality.r")

worker = new.env()
worker$defaultDemographicFile = file.path("..","data-formatted","out_demographic.csv")
worker$defaultPersonalityFile = file.path("..","data-formatted","out_personality.csv")

# Loads demographic and personality data from respective CSV files
# NOTE: We are losing a considerable amount of information by doing the inner join.
worker$load <- function(demographic_file=worker$defaultDemographicFile,
						personality_file=worker$defaultPersonalityFile,
						outer_join=FALSE) {
	demographic <- demographic$load(demographic_file)
	personality <- personality$load(personality_file)

	# join demographic and personality data on worker id
	data_merge <- merge(x=demographic,y=personality,by="worker_id", all = outer_join)

	# remove old assigment ids
    data_merge <- data_merge[,!names(data_merge) %in% c("id.y","id.x")]

	return(data_merge)
}