# 
# responses
#
# loads "what you would do?" answers by merging
# batch of files.
#
# by Paulo Gomes <pfontain@ucsc.edu>

source("utilities.R")

WORKER_RESPONSES_DEFAULT_FOLDER = file.path("..","data-raw")
WORKER_RESPONSES_DEFAULT_OUTPUT_PATH = file.path("..","data-raw","tmp_worker_responses_merged.csv")

# Loads batch result responses from separate files assuming a header
load.batch.worker.responses <- function(folder=WORKER_RESPONSES_DEFAULT_FOLDER){
	number_batches = 5
	lapply(1:number_batches,function(num){
		read.csv(file.path(folder,paste("worker_results_batch_",num,".csv",sep="")))})
}

# Suggested query:
# batch_responses <- LoadBatchResponses()
# lapply(batch_responses,names) # gets the list of columns for each batch

# List of all columns to be considered in the responses
# INSTRUCTIONS: filter columns by commenting out lines 
# Generated with Reduce(paste,(Reduce(union,lapply(batch_responses,names))),sep=",")
RESPONSES_REQUESTED_FIELDS <- c(
# "HITId","HITTypeId","Title","Description","Keywords","Reward","CreationTime","MaxAssignments","RequesterAnnotation",
# "AssignmentDurationInSeconds","AutoApprovalDelayInSeconds","Expiration","NumberOfSimilarHITs","LifetimeInSeconds",
"AssignmentId","WorkerId",
# ,"AssignmentStatus","AcceptTime","SubmitTime","AutoApprovalTime","ApprovalTime","RejectionTime", "RequesterFeedback",
# "WorkTimeInSeconds","LifetimeApprovalRate",
# "Last30DaysApprovalRate","Last7DaysApprovalRate",
# "Input.AssignmentId", # merged with Input.ScenarioAssignmentId
"Input.Scenario","Answer.R1","Answer.R2","Answer.R3",
# "Answer.comment", "Approve","Reject","Input.Batch","Answer.Similar","Input.Random","Input.BatchFile",
# "Input.Number","Input.Type","Input.Notes","Answer.Severity"
"Input.ScenarioAssignmentId"
) 

# TODO: group up constants in some non global environment
RESPONSES_COLUMN_ASSIGNMENT_ID_TO_REPLACE <- "Input.AssignmentId"
RESPONSES_COLUMN_ASSIGNMENT_ID_TO_REPLACE_WITH <- "Input.ScenarioAssignmentId"

# Merges list of batch responses
# >> functional programming paradigm
#
# batch_results: list of data frame responses
#
response.merge <- function(batch_results){
	# Relabels columns Input.AssignmentId to Input.ScenarioAssignmentId
	rename.batch <- function(batch){
		position_vector <- which(names(batch)==RESPONSES_COLUMN_ASSIGNMENT_ID_TO_REPLACE)
		has_column <- length(position_vector)!=0
		if(has_column){
			colnames(batch)[position_vector[1]] <- RESPONSES_COLUMN_ASSIGNMENT_ID_TO_REPLACE_WITH
		}
		return(batch)
	}

	return(
		# stitches all data frame rows into one data frame
		# (possible because they have the exact same columns)
		Reduce(
			rbind,
			
			# adds missing columns to a renamed response batch
			# (input scenario ids are all mapped to the same column because of renaming)
			lapply(
			
				# renames the assignment id of the batch
				# (so that there is one input scenario id label)
				lapply(batch_results,rename.batch),
				function(batch){ return(util$batch.add.missing(batch,RESPONSES_REQUESTED_FIELDS))})))
}

# Loads response batches, merges them
# and saves the data to a CSV file 
response.merge.save <- function(file= WORKER_RESPONSES_DEFAULT_OUTPUT_PATH){
	responses <- response.merge(load.batch.worker.responses())
	write.csv(responses,file=file,row.names=FALSE)
}

# Testing repeated response phenomenon
test <- function(r,scenario_id_name="Input.ScenarioAssignmentId"){
	AGGREGATE_BY = c("WorkerId","Input.ScenarioAssignmentId","Answer.R1")
	AGGREGATE_X = c("AssignmentId")
	aggregate(x = r$Answer.R1,
						by = list(worker_id=r$WorkerId,scenario_id=r[,scenario_id_name]),
						FUN = function(x){length(unique(x))})    
}

# r = read.csv("../data-raw/worker_results_batch_1.csv")
# t = test(r,"Input.AssignmentId")
# r = read.csv("../data-raw/worker_results_batch_2.csv")
# t = test(r,"Input.AssignmentId")
# r = read.csv("../data-raw/worker_results_batch_2.csv")