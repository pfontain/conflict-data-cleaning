# WARNING: Way more responses in worker responses than in response ids annotation 

from os.path import expanduser, join
import csv, collections

DATA_IN_FOLDER = '../data-raw/'
DATA_OUT_FOLDER = '../data-formatted/'
DATA_TMP_FOLDER = '../data-formatted/'

class Annotation:
    """Annotation of response to hypothetical conflict"""
    
    FEATURE_ACTIVE = 1
    FEATURE_AGRESSIVE = 2
    
    SOURCE_CORPUS = 1
    SOURCE_USER_STUDY = 2
    
    def __init__(self,scenario_id,annotator_id,feature,feature_val,response_id,response_text):
        self.scenario_id = scenario_id
        self.annotator_id = annotator_id
        self.feature = feature
        self.feature_val = feature_val
        self.response_id = response_id
        self.response_text = response_text
    
    # DEPRECATED    
    def __str__(self):
        return ",".join([self.scenario_id,self.annotator_id,str(self.feature),self.feature_val,\
                     self.response_id,"\""+self.response_text+"\""])
        
    def csv(self):
        return [self.scenario_id,self.annotator_id,self.feature,self.feature_val,self.response_id,self.response_text]
    
def annotation_feature_to_label(annotation_feature):
        return {
                    Annotation.FEATURE_ACTIVE: 'active',
                    Annotation.FEATURE_AGRESSIVE: 'aggressive',
                }[annotation_feature]

def annotation_source_to_label(annotation_source):
        return {
                    Annotation.SOURCE_CORPUS: 'corpus',
                    Annotation.SOURCE_USER_STUDY: 'user_study',
                }[annotation_source]

def run():
    generate_demographic_and_personality()
    generate_worker_responses()
    generate_annotations_all()
            
def generate_annotations_all():
    generate_annotations(feature=Annotation.FEATURE_ACTIVE,source=Annotation.SOURCE_CORPUS)
    generate_annotations(feature=Annotation.FEATURE_AGRESSIVE,source=Annotation.SOURCE_CORPUS)
    generate_annotations(feature=Annotation.FEATURE_ACTIVE,source=Annotation.SOURCE_USER_STUDY)
    generate_annotations(feature=Annotation.FEATURE_AGRESSIVE,source=Annotation.SOURCE_USER_STUDY)
    
def generate_annotations(feature,source):
    annotation_feature_label = annotation_feature_to_label(feature)
    annotation_source = annotation_source_to_label(source)
    in_annotations_filename = "annotations_"+annotation_feature_label+"_" + annotation_source + ".csv"
    out_annotations_filename = "tmp_annotations_"+annotation_feature_label+"_" + annotation_source + ".csv"
    
    in_annotations_file_path = join(expanduser(DATA_IN_FOLDER),in_annotations_filename)
    out_annotations_file_path = join(expanduser(DATA_OUT_FOLDER),out_annotations_filename)
    
    num_annotations_per_row = 7
    
    annotations = []
    annotation_labels = ["scenario_id","annotator_id","feature","feature_val","response_id","response_text"]
    
    with open(in_annotations_file_path, 'r') as in_annotations:
        annotations_reader = csv.DictReader(in_annotations)

        for row in annotations_reader:
            for i in range(1,num_annotations_per_row+1):
                annotation = Annotation(scenario_id=row["Input.ScenarioId"],\
                                    annotator_id=row["WorkerId"],\
                                    feature=feature,\
                                    feature_val=row["Answer.q"+str(i)],\
                                    response_id=row["Input.response_id_"+str(i)],\
                                    response_text=row["Input.response_"+str(i)])
                annotations.append(annotation)
            
    with open(out_annotations_file_path, 'w') as out_annotations_file:
        csv_writer = csv.writer(out_annotations_file,delimiter=',',quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        csv_writer.writerow(annotation_labels) 
                    
        for a in annotations:
            csv_writer.writerow([a.scenario_id,a.annotator_id,a.feature,a.feature_val,a.response_id,a.response_text])
            
    return annotations
    
def generate_worker_responses():
    in_worker_responses_filename = "tmp_worker_responses_merged.csv"
    out_worker_responses_filename = "tmp_worker_responses.csv"
    
    in_worker_responses_file_path = join(expanduser(DATA_IN_FOLDER),in_worker_responses_filename)
    out_worker_responses_file_path = join(expanduser(DATA_OUT_FOLDER),out_worker_responses_filename)
    
    worker_labels = ['worker_id','scenario_id','text']
    WorkerResponse = collections.namedtuple('WorkerResponse',worker_labels)
    worker_responses = []
    
    with open(in_worker_responses_file_path, 'r') as in_worker_responses:
        responses_reader = csv.DictReader(in_worker_responses)
        # skip header
        # next(responses_reader)
        for row in responses_reader:
            # only including first answer
            worker_responses.append(WorkerResponse(worker_id=row["WorkerId"],scenario_id=row["Input.ScenarioAssignmentId"],text=row["Answer.R1"]))
            
    with open(out_worker_responses_file_path, 'w') as out_worker_responses_file:
        csv_writer = csv.writer(out_worker_responses_file,delimiter=',',quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        csv_writer.writerow(worker_labels) 
                    
        # scenarios are reordered
        for worker_response in worker_responses:
            csv_writer.writerow([worker_response.worker_id,worker_response.scenario_id,worker_response.text])
            
    return worker_responses

def generate_demographic_and_personality():
    in_qualifications_filename = "qualifications.csv"
    out_demographic_filename = "out_demographic.csv"
    out_personality_filename = "out_personality.csv"
    qualifications_demographic_ID = "2F5GJ2D21O3WIZGG97GSN2KYEQHD71"
    
    in_qualifications_file_path = join(expanduser(DATA_IN_FOLDER),in_qualifications_filename)
    out_demographic_file_path = join(expanduser(DATA_OUT_FOLDER),out_demographic_filename)
    out_personality_file_path = join(expanduser(DATA_OUT_FOLDER),out_personality_filename)
    
    # List of header labels for demographic data file
    demographic_labels=["id","worker_id","city","age_group","gender","education","n_mobile_numbers","n_text_messages","n_friends_sn","n_hours_pa","n_hours_vg_day","n_hours_tv_week"];
    city_original_field_position = 8
    remaining_field_positions_after_city = 14
    
    # List of header labels for personality data file
    personality_labels=["id","worker_id","bf1_reserved","bf2_trusting","bf3_lazy","bf4_relaxed","bf5_artistic","bf6_outgoing","bf7_fault_others","bf8_thorough","bf9_nervous","bf10_imagination","bf11_kind"];
    numbered_features_start_position = 3
    
    with open(out_demographic_file_path,'w') as out_demographic_file, \
        open(out_personality_file_path,'w') as out_personality_file, \
        open(in_qualifications_file_path,'r') as qualifications_file:
        
        out_demographic_file.write(",".join(demographic_labels)+'\n')
        out_personality_file.write(",".join(personality_labels)+'\n')
    
        for line in qualifications_file:
            line_split = line.split()
            
            if line_split[0] == qualifications_demographic_ID: # demographic line  
                city = " ".join(line_split[city_original_field_position:-remaining_field_positions_after_city])
                new_line = ",".join(line_split[1:numbered_features_start_position]) + \
                            ",\"" + city + "\"," + \
                            ",".join(line_split[numbered_features_start_position+1:city_original_field_position-1:2]+ \
                                     line_split[-remaining_field_positions_after_city+1::2]) + \
                            "\n"
                out_demographic_file.write(new_line)
                
            else: # personality line
                new_line = ",".join(line_split[1:numbered_features_start_position]+line_split[numbered_features_start_position+1::2])+'\n'
                out_personality_file.write(new_line)
