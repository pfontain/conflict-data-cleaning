The Project organizes and merges data gathered by Reid Swanson
in several crowd sourcing tasks. It also contains scripts for
automated statistical hypothesis testing, classifiers for annotation
labels, and exportation of data to Probabilistic Soft Logic (PSL)
friendly format. The project is mostly written in R.

For more information please consult the wiki
accessible through the sidebar or the link below:
https://bitbucket.org/pfontain/conflict-data-cleaning/wiki/Home