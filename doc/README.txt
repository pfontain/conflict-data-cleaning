# Data
# Describes the generated demographic and personality data
# Describes the "What you would do?" data
# Additionally it suggest some queries
# written in R friendly format

###########################
#                         #
# Demographic data fields #
#                         #
###########################

# id: demographic data id

# worker_id

# city: largest city within 20 miles of where worker lives
# would be good to map to country if possible

# gender
# WARNING: gender percentages different from other demographic data

# age_group

# education: highest level of education achieved
# ? two high school levels of education ?
# setting both to unknown and mapping the rest accordingly

# n_mobile_numbers: number range of phone numbers on mobile phone

# n_text_messages: number range of text messages received per week
# is the participant able to remember or check this easily?

# n_friends_sn: number range friends on social networks

# n_hours_pa: number range of physical activity hours per week

# n_hours_vg_day: number range of video game hours per day on average

# n_hours_tv_week: number range of tv hours per day on average
# TODO: Change misleading name

###########################
#                         #
# Personality data fields #
#                         #
###########################

# id: personality data id
# worker_id

# bf_*: self reported bf items
# 1: I see myself as someone who is reserved
# 2: I see myself as someone who is generally trusting
# 3: I see myself as someone who tends to be lazy
# 4: I see myself as someone who is relaxed, handles stress well
# 5: I see myself as someone who has few artistic interests
# 6: I see myself as someone who is outgoing, sociable
# 7: I see myself as someone who tends to find fault with others
# 8: I see myself as someone who does a thorough job
# 9: I see myself as someone who gets nervous easily
# 10: I see myself as someone who has an active imagination
# 11: I see myself as someone who is considerate and kind to almost everyone
# All entries are likert scale from 1 (Disagree strongly) to 5 (Agree strongly)

###########################
#                         #
# Demographic data fields #
#                         #
###########################

# Different batch files have different number of columns
# List of columns not common to all
# [1] "Input.Batch"                "Answer.Similar"             "Input.Random"               "Input.BatchFile"           
# [5] "Input.ScenarioAssignmentId" "Input.Number"               "Input.Type"                 "Input.Notes"               
# [9] "Answer.Severity"            "Input.AssignmentId"

###########################
#                         #
# Suggested Queries       #
#                         #
###########################

# suggested crosstables
# table(demographic$gender,demographic$n_mobile_numbers)
# table(demographic$gender,demographic$n_mobile_numbers)
# table(demographic$gender,demographic$n_hours_vg_day)
# table(demographic$gender,demographic$n_hours_pa)
# table(demographic$gender,demographic$age_group)

# more queries
# table(data_merge$bf3_lazy,data_merge$age_group)
# t_lazy_age <- table(data_merge$bf3_lazy,data_merge$age_group)
# prop.table(t_lazy_age,1)
# prop.table(t_lazy_age,2)
# summary(data_merge$bf3_lazy)
# t_reserved_outgoing <- table(data_merge$bf1_reserved,data_merge$bf6_outgoing)
# t_reserved_outgoing
# prop.table(t_reserved_outgoing)

# Initial notes
# people lean towards the more acceptable answer of the self reports.
# younger people have an easier time saying that they are lazy
# a considerable amount of people say that they are "a little reserved"
# and "a little outgoing"